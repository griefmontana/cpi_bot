from typing import Optional

from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware

from misc import bot

from models.telegram import Chat
from models.telegram import User


class ACLMiddleware(BaseMiddleware):
    async def setup_chat(self, data: dict, user: types.User, chat: Optional[types.Chat] = None):
        user_id = user.id
        chat_id = chat.id if chat else user.id

        base_user = await User.get(user_id)
        if base_user is None:
            base_user = await User.create(id=user_id, first_name=user.first_name, last_name=user.last_name,
                                          username=user.username)
        else:
            await base_user.update(
                first_name=user.first_name, last_name=user.last_name, username=user.username).apply()
        base_chat = await Chat.get(chat_id)
        if base_chat is None:
            base_chat = await Chat.create(id=chat_id, title=chat.title, username=chat.username)
        else:
            await base_chat.update(title=chat.title, username=chat.username).apply()

        data['user'] = base_user
        data['chat'] = base_chat
        data['bot'] = bot

    async def on_pre_process_message(self, message: types.Message, data: dict):
        if message.new_chat_members:
            user = message.new_chat_members[0]
        else:
            user = message.from_user
        await self.setup_chat(data, user, message.chat)

    async def on_pre_process_callback_query(self, query: types.CallbackQuery, data: dict):
        await self.setup_chat(data, query.from_user, query.message.chat if query.message else None)

import datetime
import enum
from sqlalchemy.dialects.postgresql import ENUM

from .base import db, TimedBaseModel
from .telegram import ChatRelatedModel, UserRelatedModel


class ReportType(enum.Enum):
    morning = 1
    evening = 2


class Reporter(TimedBaseModel, ChatRelatedModel, UserRelatedModel):
    __tablename__ = 'reporters'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    is_enabled = db.Column(db.Boolean, default=True)


def get_first_day():
    return datetime.datetime.utcnow().replace(day=5, hour=23, minute=0,
                                              second=0, microsecond=0)


def get_last_day():
    return datetime.datetime.utcnow().replace(day=26, hour=23, minute=0,
                                              second=0, microsecond=0)


class ChatSetting(TimedBaseModel, ChatRelatedModel):
    __tablename__ = 'chat_settings'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    is_enabled = db.Column(db.Boolean, default=False)
    first_day = db.Column(db.DateTime(True), default=get_first_day)
    last_day = db.Column(db.DateTime(True), default=get_last_day)


class ReportPeriod(TimedBaseModel, ChatRelatedModel):
    __tablename__ = 'report_periods'

    TYPES = [
        ('morning', 'Morning'),
        ('regular-user', 'Evening')
    ]

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    type = db.Column(ENUM(ReportType, create_type=False))
    start = db.Column(db.DateTime(True), nullable=False)
    end = db.Column(db.DateTime(True), nullable=False)


class Report(TimedBaseModel):
    __tablename__ = 'reports'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    period_id = db.Column(
        db.ForeignKey(f"{ReportPeriod.__tablename__}.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    reporter_id = db.Column(
        db.ForeignKey(f"{Reporter.__tablename__}.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    text = db.Column(db.Text(), nullable=False)

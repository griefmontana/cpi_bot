Install dependencies

    libpq-dev
    python3-dev
    gcc
    redis-server
    postgresql postgresql-contrib
    python3.8-dev

Running alembic migrations
1. export PYTHONPATH=$PYTHONPATH:/path/to/bot
2. alembic upgrade head

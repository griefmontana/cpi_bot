import logging
import settings

from loguru import logger
from notifiers.logging import NotificationHandler


class InterceptHandler(logging.Handler):
    LEVELS_MAP = {
        logging.CRITICAL: "CRITICAL",
        logging.ERROR: "ERROR",
        logging.WARNING: "WARNING",
        logging.INFO: "INFO",
        logging.DEBUG: "DEBUG",
    }

    def _get_level(self, record):
        return self.LEVELS_MAP.get(record.levelno, record.levelno)

    def emit(self, record):
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(self._get_level(record), record.getMessage())


def setup():
    logging.basicConfig(handlers=[InterceptHandler()], level=settings.LOGGING_LEVEL)
    logger.disable("sqlalchemy.engine.base")
    params = {
        'token': settings.BOT_TOKEN,
        'chat_id': settings.BOT_OWNER_ID,
    }
    telegram_handler = NotificationHandler("telegram", defaults=params)
    logger.add("logs/bot_{time}.log", retention="30 days", rotation="1 day", level='INFO')
    logger.add(telegram_handler, level='ERROR')

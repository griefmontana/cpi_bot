import settings

from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.redis import RedisStorage

storage = RedisStorage('localhost', settings.REDIS_PORT, db=5)

bot = Bot(token=settings.BOT_TOKEN)

dp = Dispatcher(bot, storage=storage)


async def on_shutdown(dispatcher: Dispatcher):
    await dp.storage.close()
    await dp.storage.wait_closed()


def storage_setup(executor):
    executor.on_shutdown(on_shutdown)

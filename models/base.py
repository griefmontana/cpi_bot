import typing as ty
import sqlalchemy as sa
import settings

from aiogram import Dispatcher
from aiogram.utils.executor import Executor
from gino import Gino


db = Gino()


class BaseModel(db.Model):
    __abstract__ = True

    def __str__(self):
        model = self.__class__.__name__
        table: sa.Table = sa.inspect(self.__class__)
        primary_key_columns: ty.List[sa.Column] = table.primary_key.columns
        values = {
            column.name: getattr(self, self._column_name_map[column.name])
            for column in primary_key_columns
        }
        values_str = " ".join(f"{name}={value!r}"
                              for name, value in values.items())
        return f"<{model} {values_str}>"


class TimedBaseModel(BaseModel):
    __abstract__ = True

    created = db.Column(db.DateTime(True), server_default=db.func.now())
    updated = db.Column(db.DateTime(True), server_default=db.func.now())


async def on_startup(dispatcher: Dispatcher):
    await db.set_bind(settings.POSTGRES_URI)
    await db.gino.create_all()


async def on_shutdown(dispatcher: Dispatcher):
    bind = db.pop_bind()
    if bind:
        await bind.close()


def setup(executor: Executor):
    executor.on_startup(on_startup)
    executor.on_shutdown(on_shutdown)

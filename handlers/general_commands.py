import typing as ty
from aiogram import types
import datetime
from services.scheduler import scheduler
from loguru import logger

from misc import dp
from models import Chat, ChatSetting, User, Report, Reporter, ReportPeriod
from utils.helpers import create_report_periods, get_chat_config, get_hour_minute
from utils.jobs import (
    notify_day_before_job, notify_evening_job, notify_morning_job, notify_rating_job, notify_rules_job
)


async def get_chat_settings(chat_id: int) -> ty.Optional[ChatSetting]:
    return await ChatSetting.query.where(ChatSetting.chat_id == chat_id).gino.first()


async def get_reporter(chat_id: int, user_id: int) -> ty.Optional[Reporter]:
    return await Reporter.query.where(Reporter.chat_id == chat_id).where(Reporter.user_id == user_id).gino.first()


async def get_period(chat_id: int, period_type: str) -> ty.Optional[ReportPeriod]:
    current_datetime = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
    return await ReportPeriod.query.where(ReportPeriod.chat_id == chat_id).where(
        ReportPeriod.start <= current_datetime).where(
        ReportPeriod.end >= current_datetime).where(ReportPeriod.type == period_type).gino.first()


async def get_report(reporter_id: int, period_id: int) -> ty.Optional[Report]:
    return await Report.query.where(Report.reporter_id == reporter_id).where(
        Report.period_id == period_id).gino.first()


async def create_report(period_id, reporter_id, text):
    await Report.create(period_id=period_id, reporter_id=reporter_id, text=text)


@dp.message_handler(chat_type=[types.ChatType.GROUP, types.ChatType.SUPERGROUP],
                    commands=['start'])
async def process_start_command(message: types.Message, chat: Chat):
    await message.delete()
    chat_setting = await get_chat_settings(chat.id)
    chat_config = get_chat_config(chat.id)
    if chat_setting:
        if chat_setting.is_enabled:
            await message.answer("Бот уже готов к работе")
        else:
            await chat_setting.update(is_enabled=True).apply()
            await message.answer("Бот активирован")
    else:
        await create_report_periods(chat)
        now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
        # Add notify day before job
        day_before_hour, day_before_minute = get_hour_minute(chat_config['NOTIFICATION_DAY_BEFORE_TIME'])
        day_before_hour -= chat_config['CHAT_TIMEZONE']
        day_before_day = chat_config['NOTIFICATION_DAY_BEFORE_DAY']
        if day_before_hour < 0:
            day_before_hour = 24 + day_before_hour
            day_before_day -= 1

        logger.info(f"Creating day before notification on {day_before_day} {day_before_hour}:{day_before_minute}"
                    f"for Chat {chat.title} [{chat.id}]")
        scheduler.add_job(
            notify_day_before_job, trigger='cron',
            hour=day_before_hour,
            minute=day_before_minute,
            day=day_before_day,
            month=now.month,
            year=now.year,
            kwargs={'chat_id': chat.id}
        )

        # Add rules notification
        rules_hour, rules_minute = get_hour_minute(chat_config['NOTIFICATION_RULES_TIME'])
        rules_hour -= chat_config['CHAT_TIMEZONE']
        rules_day = chat_config['NOTIFICATION_RULES_DAY']
        if rules_hour < 0:
            rules_hour = 24 + rules_hour
            rules_day -= 1

        logger.info(f"Creating rules notification on {rules_day} {rules_hour}:{rules_minute}"
                    f"for Chat {chat.title} [{chat.id}]")
        scheduler.add_job(
            notify_rules_job, trigger='cron',
            hour=rules_hour,
            minute=rules_minute,
            day=rules_day,
            month=now.month,
            year=now.year,
            kwargs={'chat_id': chat.id}
        )

        # Add rating notification
        rating_hour, rating_minute = get_hour_minute(chat_config['NOTIFICATION_RATING_TIME'])
        rating_hour -= chat_config['CHAT_TIMEZONE']
        rating_day = f"{chat_config['FIRST_DAY']}-{chat_config['LAST_DAY']}"
        if rating_hour < 0:
            rating_hour = 24 + rating_hour
            rating_day = f"{chat_config['FIRST_DAY']-1}-{chat_config['LAST_DAY']}"
        rating_days_of_week = chat_config['NOTIFICATION_RATING_DAY_WEEK']
        logger.info(f"Creating rating notification on days of week {rating_days_of_week},"
                    f"{rating_hour}:{rating_minute} for Chat {chat.title} [{chat.id}]")
        scheduler.add_job(
            notify_rating_job, trigger='cron',
            hour=rating_hour,
            minute=rating_minute,
            day_of_week=rating_days_of_week,
            day=rating_day,
            month=now.month,
            year=now.year,
            kwargs={'chat_id': chat.id}
        )

        # Add morning notification
        # morning_hour, morning_minute = get_hour_minute(chat_config['NOTIFICATION_MORNING_TIME'])
        # morning_hour -= chat_config['CHAT_TIMEZONE']
        # morning_day = f"{chat_config['FIRST_DAY']}-{chat_config['LAST_DAY']}"
        # if morning_hour < 0:
        #     morning_hour = 24 + morning_hour
        #     morning_day = f"{chat_config['FIRST_DAY']-1}-{chat_config['LAST_DAY']}"
        # morning_days_of_week = chat_config['NOTIFICATION_MORNING_DAY_WEEK']
        # logger.info(f"Creating morning notification on days of week {morning_days_of_week},"
        #             f"{morning_hour}:{morning_minute} for Chat {chat.title} [{chat.id}]")
        # scheduler.add_job(
        #     notify_morning_job, trigger='cron',
        #     hour=morning_hour,
        #     minute=morning_minute,
        #     day_of_week=morning_days_of_week,
        #     day=morning_day,
        #     month=now.month,
        #     year=now.year,
        #     kwargs={'chat_id': chat.id}
        # )

        # Add evening notification
        evening_hour, evening_minute = get_hour_minute(chat_config['NOTIFICATION_EVENING_TIME'])
        evening_hour -= chat_config['CHAT_TIMEZONE']
        evening_day = f"{chat_config['FIRST_DAY']}-{chat_config['LAST_DAY']}"
        if evening_hour < 0:
            evening_hour = 24 + evening_hour
            evening_day = f"{chat_config['FIRST_DAY'] - 1}-{chat_config['LAST_DAY']}"
        evening_days_of_week = chat_config['NOTIFICATION_EVENING_DAY_WEEK']
        logger.info(f"Creating evening notification on days of week {evening_days_of_week},"
                    f"{evening_hour}:{evening_minute} for Chat {chat.title} [{chat.id}]")
        scheduler.add_job(
            notify_evening_job, trigger='cron',
            hour=evening_hour,
            minute=evening_minute,
            day_of_week=evening_days_of_week,
            day=evening_day,
            month=now.month,
            year=now.year,
            kwargs={'chat_id': chat.id}
        )

        await ChatSetting.create(chat_id=chat.id, is_enabled=True)
        logger.info("Start procedure for Chat {chat.title} [{chat.id}] is done")
        await message.answer("Бот активирован")


@dp.message_handler(commands=['stop'], chat_type=[types.ChatType.GROUP, types.ChatType.SUPERGROUP])
async def process_stop_command(message: types.Message, chat: Chat, user: User):
    chat_setting = await get_chat_settings(chat.id)
    if chat_setting:
        if chat_setting.is_enabled:
            await chat_setting.update(is_enabled=False).apply()
            await message.answer("Бот отключён")
        else:
            await message.answer("Бот уже отключён")
    else:
        await message.answer("Бот ещё не был активирован")


@dp.message_handler(commands=['subscribe'], chat_type=[types.ChatType.GROUP, types.ChatType.SUPERGROUP],
                    chat_enabled=True)
async def process_subscribe_command(message: types.Message, chat: Chat, user: User):
    reporter = await get_reporter(chat.id, user.id)
    if reporter:
        await reporter.update(is_enabled=True).apply()
    if not reporter:
        await Reporter.create(chat_id=chat.id, user_id=user.id)
        await message.reply("Приветствуем тебя на пути к цели 🥳")


@dp.message_handler(commands=['rules'], chat_type=[types.ChatType.GROUP, types.ChatType.SUPERGROUP], chat_enabled=True)
async def process_rules_command(message: types.Message, chat: Chat, user: User):
    await notify_rules_job(chat.id)


@dp.message_handler(content_types=types.ContentTypes.NEW_CHAT_MEMBERS, chat_enabled=True)
async def process_join(message: types.Message, chat: Chat, user: User):
    reporter = await get_reporter(chat.id, user.id)
    if reporter:
        await Reporter.update(is_enabled=True).apply()
        await message.reply('Ты снова с нами на пути к цели 🥳')
    if not reporter:
        await Reporter.create(chat_id=chat.id, user_id=user.id)
        await message.reply("Приветствуем тебя на пути к цели 🥳")


@dp.message_handler(content_types=types.ContentTypes.LEFT_CHAT_MEMBER)
async def left_chat_member(message: types.Message, chat: Chat, user: User):
    reporter = await get_reporter(chat.id, user.id)
    if reporter.is_enabled:
        await reporter.update(is_enabled=False).apply()
        await message.answer(":(")


@dp.message_handler(chat_enabled=True, content_types=types.ContentTypes.ANY)
async def process_text(message: types.Message, chat: Chat, user: User):
    chat_config = get_chat_config(chat.id)
    if f'#{chat_config["PLAN_KEYWORD"]}' in message.text:
        reporter = await get_reporter(chat.id, user.id)
        if not reporter:
            await message.reply('Тебя нет в списке. Нажми /subscribe')
        else:
            period = await get_period(chat.id, 'morning')
            if not period:
                await message.reply('Ты не можешь сейчас планировать день')
            else:
                report = await get_report(reporter.id, period.id)
                if report:
                    await message.reply('Вы уже отправили отчёт о планировании дня. Больше не нужно')
                else:
                    await create_report(period.id, reporter.id, message.text)
                    await message.reply("Отчёт принят")
    elif f'#{chat_config["DONE_KEYWORD"]}' in message.text:
        reporter = await get_reporter(chat.id, user.id)
        if not reporter:
            await message.reply('Тебя нет в списке. Нажми /subscribe')
        else:
            period = await get_period(chat.id, 'evening')
            if not period:
                await message.reply('Ты не можешь сейчас сдавать отчёт за день')
            else:
                report = await get_report(reporter.id, period.id)
                if not report:
                    await create_report(period.id, reporter.id, message.text)
                    await message.reply("Отчёт принят")


@dp.message_handler(content_types=types.ContentTypes.ANY)
async def process_listener(message: types.Message, chat: Chat, user: User):
    pass


@dp.errors_handler()
async def errors_handler(update: types.Update, exception: Exception):
    try:
        raise exception
    except Exception as e:
        logger.exception("Cause exception {e} in update {update}", e=e, update=update)
    return True

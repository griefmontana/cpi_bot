from aiogram.utils.executor import Executor
from loguru import logger   # NOQA
from filters import setup as filter_setup
from services.scheduler import setup as scheduler_setup
from misc import dp, storage_setup
from models.base import setup as db_setup
from middlewares import setup as middleware_setup
from utils.logging import setup as logging_setup


if __name__ == '__main__':
    runner = Executor(dp)
    db_setup(runner)
    storage_setup(runner)
    scheduler_setup(runner)
    middleware_setup(dp)
    filter_setup(dp)
    logging_setup()
    import handlers  # NOQA
    runner.start_polling()

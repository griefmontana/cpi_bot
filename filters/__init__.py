from aiogram.dispatcher import Dispatcher

from .chat_enabled import IsChatEnabled


def setup(dispatcher: Dispatcher):
    dispatcher.filters_factory.bind(IsChatEnabled)

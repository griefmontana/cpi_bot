from aiogram import types
from aiogram.dispatcher.filters import BoundFilter
from dataclasses import dataclass

from models.reporters import ChatSetting


@dataclass
class IsChatEnabled(BoundFilter):
    key = 'chat_enabled'

    chat_enabled: bool

    async def check(self, message: types.Message):
        chat_id = message.chat.id
        chat_setting = await ChatSetting.query.where(ChatSetting.chat_id == chat_id).where(
            ChatSetting.is_enabled == True).gino.all() # NOQA
        return True if chat_setting else False

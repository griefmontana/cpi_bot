import datetime
from jinja2 import Environment, FileSystemLoader
from sqlalchemy import or_

from misc import bot
from models.telegram import User
from utils.helpers import get_user_tag, get_chat_config
# Reminder 11:30 (UTC - 9:30)
# Reminder 23:30 (UTC - 21:30)
# Rating the next day (if day before was period and current period active
# 12:00 (9:00) refresh periods
# 24:00 (22:00) refresh periods

from models.reporters import Report, Reporter, ReportPeriod


async def get_periods(chat_id, start, end):
    return await ReportPeriod.query.where(ReportPeriod.chat_id == chat_id).where(
        ReportPeriod.start >= start).where(ReportPeriod.end <= end).gino.all()


async def get_period(chat_id, date, period_type):
    return await ReportPeriod.query.where(ReportPeriod.chat_id == chat_id).where(
        ReportPeriod.start <= date).where(ReportPeriod.end >= date).where(
        ReportPeriod.type == period_type).gino.first()


async def get_reports(reporter_id, morning_period, evening_period):
    return await Report.query.where(Report.reporter_id == reporter_id).where(
        or_(Report.period_id == morning_period.id, Report.period_id == evening_period.id)).gino.all()


async def get_reporters(chat_id):
    return await Reporter.query.where(
        Reporter.chat_id == chat_id).where(Reporter.is_enabled == True).gino.all()    # NOQA


async def get_period_reports(period_id):
    return await Report.query.where(Report.period_id == period_id).gino.all()


async def notify_day_before_job(chat_id: int):
    with open('utils/texts/first_notify.txt', 'r') as f:
        text = f.read()
    await bot.send_message(chat_id=chat_id, text=text)


async def notify_rules_job(chat_id: int):
    chat_config = get_chat_config(chat_id)
    file_loader = FileSystemLoader('utils/texts')
    env = Environment(loader=file_loader)
    template = env.get_template('rules.txt')
    text = template.render(plan_keyword=chat_config['PLAN_KEYWORD'], done_keyword=chat_config['DONE_KEYWORD'])
    await bot.send_message(chat_id=chat_id, text=text)


async def notify_morning_job(chat_id: int):
    """
    Notification about plan report on evening (by default at 11:30)
    """
    now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

    period = await get_period(chat_id, now, 'morning')
    reports = await get_period_reports(period.id)
    reporter_ids = [report.reporter_id for report in reports]

    reporters = await get_reporters(chat_id)
    not_sent = []

    for reporter in reporters:
        user = await User.get(reporter.user_id)
        user_tag = get_user_tag(user)
        if reporter.id not in reporter_ids:
            not_sent.append(user_tag)

    file_loader = FileSystemLoader('utils/texts')
    env = Environment(loader=file_loader)
    template = env.get_template('morning.txt')
    text = template.render(not_sent=not_sent)
    await bot.send_message(chat_id=chat_id, text=text, parse_mode='markdown')


async def notify_evening_job(chat_id: int):
    """
    Notification about done report on evening (by default at 23:30)
    """
    now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

    period = await get_period(chat_id, now, 'evening')
    reports = await get_period_reports(period.id)
    reporter_ids = [report.reporter_id for report in reports]

    reporters = await get_reporters(chat_id)

    not_sent = []

    for reporter in reporters:
        user = await User.get(reporter.user_id)
        user_tag = get_user_tag(user)
        if reporter.id not in reporter_ids:
            not_sent.append(user_tag)

    file_loader = FileSystemLoader('utils/texts')
    env = Environment(loader=file_loader)
    template = env.get_template('evening.txt')
    text = template.render(not_sent=not_sent)
    await bot.send_message(chat_id=chat_id, text=text, parse_mode='markdown')


async def notify_rating_job(chat_id: int):
    """
    Job about rating for previous day
    """

    chat_config = get_chat_config(chat_id)
    # TODO: Check validation of days
    today = datetime.datetime.utcnow() + datetime.timedelta(hours=chat_config['CHAT_TIMEZONE'])
    today_start = today.replace(
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
        tzinfo=datetime.timezone.utc
    ) - datetime.timedelta(hours=chat_config['CHAT_TIMEZONE'])
    today_end = today_start + datetime.timedelta(days=1)

    if today.weekday() > 4 and not chat_config['HOLIDAYS']:
        return
    else:
        if today.weekday() == 0 and not chat_config['HOLIDAYS']:
            days_shift = 3
        else:
            days_shift = chat_config['CHAT_RATING_DAYS_SHIFT']

        # TODO:Refactor
        rating_start = today_start - datetime.timedelta(days=days_shift)
        rating_end = today_end - datetime.timedelta(days=days_shift)
        periods = await get_periods(chat_id, rating_start, rating_end)
        if not periods:
            return
        reporters = await Reporter.query.where(Reporter.chat_id == chat_id).gino.all()
        total_list = []
        # TODO: Configurable
        total = 1
        for reporter in reporters:
            user = await User.get(reporter.user_id)
            reports = await get_reports(reporter.id, periods[0], periods[1])
            total_list.append(
                {'tag': get_user_tag(user), 'sent': len(reports), 'not_sent': total - len(reports)}
            )

        sent_users = [user for user in total_list if user['sent']]
        not_sent_users = [user for user in total_list if user['not_sent']]
        file_loader = FileSystemLoader('utils/texts')
        env = Environment(loader=file_loader)
        template = env.get_template('rating.txt')
        text = template.render(sent_users=sent_users, not_sent_users=not_sent_users)
        await bot.send_message(chat_id=chat_id, text=text, parse_mode='markdown')

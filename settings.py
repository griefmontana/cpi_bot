import os
from pathlib import Path
import yaml
from decouple import config


POSTGRES_URI = config('POSTGRES_URI')
BOT_TOKEN = config('BOT_TOKEN')
REDIS_DB_JOBSTORE = config('REDIS_DB_JOBSTORE')
REDIS_HOST = config('REDIS_HOST')
REDIS_PORT = config('REDIS_PORT')
LOGGING_LEVEL = config('LOGGING_LEVEL')
BOT_OWNER_ID = config('BOT_OWNER_ID')


def configure_yaml():
    configs = {}
    with open('chat_settings/default.yml', 'rb') as default_yaml:
        default_config = yaml.load(default_yaml, Loader=yaml.CLoader)

    configs.update(default_config)
    for root, dirs, files in os.walk('chat_settings'):
        for folder_file in files:
            if os.path.splitext(folder_file)[1] == '.yml' and folder_file != 'default.yml':
                with open(str(Path('chat_settings').joinpath(folder_file)), 'rb') as yaml_config:
                    chat_config = yaml.load(yaml_config, Loader=yaml.CLoader)
                    assert default_config[list(default_config.keys())[0]].keys() == chat_config[
                        list(chat_config.keys())[0]].keys()
                    assert isinstance(list(chat_config.keys())[0], int)
                    configs.update(chat_config)
    return configs


CHAT_SETTINGS = configure_yaml()

from datetime import datetime, date, timedelta, timezone
from calendar import monthrange
from loguru import logger

from settings import CHAT_SETTINGS
from models.reporters import ReportPeriod
from models.telegram import Chat


def get_chat_config(chat_id: int):
    return CHAT_SETTINGS[chat_id] if chat_id in CHAT_SETTINGS else CHAT_SETTINGS['default']


async def get_period(chat_id:int, start:datetime, end:datetime, period_type:str):
    return await ReportPeriod.query.where(ReportPeriod.chat_id == chat_id).where(
        ReportPeriod.start == start).where(
        ReportPeriod.end == end).where(ReportPeriod.type == period_type).gino.first()


async def create_period(chat_id, start, end, period_type):
    await ReportPeriod.create(type=period_type, chat_id=chat_id, start=start, end=end)


def get_hour_minute(h_m_timestamp: str):
    hour, minute = list(map(int, h_m_timestamp.split(":")))
    hour = hour % 24
    minute = minute % 60
    return hour, minute


async def create_report_periods(chat: Chat) -> None:
    chat_id = chat.id

    config = get_chat_config(chat_id)
    # TODO: Configurable via ChatSettings
    first_day = config['FIRST_DAY']
    last_day = config['LAST_DAY']
    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    morning_start_hour, morning_start_minute = get_hour_minute(config['MORNING_START_TIME'])
    morning_end_hour, morning_end_minute = get_hour_minute(config['MORNING_END_TIME'])
    evening_start_hour, evening_start_minute = get_hour_minute(config['EVENING_START_TIME'])
    evening_end_hour, evening_end_minute = get_hour_minute(config['EVENING_END_TIME'])
    current_month = now.month
    current_year = config['CHAT_YEAR']

    logger.info(f"Started creating report periods for Chat={chat.title}[{chat.id}]")

    working_days = []
    # 1. Count all working days
    for current_day in range(first_day, last_day + 1):
        day = date(day=current_day, month=current_month, year=current_year)
        if day.weekday() < 5:
            logger.debug(f"{day} working day")
            working_days.append(current_day)
        elif config['HOLIDAYS']:
            working_days.append(current_day)

    logger.info(f"For Chat={chat.title}[{chat.id}] found {len(working_days)} working days {working_days}")

    for current_day in working_days:
        # TODO: Refactor this
        morning_start_datetime = datetime(
            day=current_day,
            month=current_month,
            year=current_year,
            hour=morning_start_hour,
            minute=morning_start_minute,
            tzinfo=timezone.utc) - timedelta(hours=config['CHAT_TIMEZONE'])

        morning_end_day = current_day + 1 if morning_end_hour == 0 else current_day
        morning_end_datetime = datetime(
            day=morning_end_day,
            month=current_month,
            year=current_year,
            hour=morning_end_hour,
            minute=morning_end_minute,
            tzinfo=timezone.utc) - timedelta(hours=config['CHAT_TIMEZONE'])

        evening_start_datetime = datetime(
            day=current_day,
            month=current_month,
            year=current_year,
            hour=evening_start_hour,
            minute=evening_start_minute,
            tzinfo=timezone.utc) - timedelta(hours=config['CHAT_TIMEZONE'])
        if evening_end_hour == 0:
            evening_end_day = current_day + 1
            if evening_end_day > monthrange(now.year, now.month)[1]:
                evening_end_day = 1
                evening_end_month = current_month + 1
            else:
                evening_end_month = current_month
        else:
            evening_end_day = current_day
            evening_end_month = current_month
        evening_end_datetime = datetime(
            day=evening_end_day,
            month=evening_end_month,
            year=current_year,
            hour=evening_end_hour,
            minute=evening_end_minute,
            tzinfo=timezone.utc) - timedelta(hours=config['CHAT_TIMEZONE'])

        # morning_period = await get_period(chat_id, morning_start_datetime, morning_end_datetime, 'morning')
        # if not morning_period:
        #     await create_period(chat_id, morning_start_datetime, morning_end_datetime, 'morning')
        #     logger.info(f"Morning period  {morning_start_datetime} for Chat {chat.title}[{chat.id}] created")

        evening_period = await get_period(chat_id, evening_start_datetime, evening_end_datetime, 'evening')
        if not evening_period:
            await create_period(chat_id, evening_start_datetime, evening_end_datetime, 'evening')
            logger.info(f"Evening period {evening_start_datetime} for Chat {chat.title}[{chat.id}] created")


def get_user_tag(user):
    if user.username:
        tag = f'@{user.username}'
    else:
        tag = f'{user.first_name} {user.last_name or ""}'.strip()
    return f"[{tag}](tg://user?id={user.id})"

from aiogram import Dispatcher
from aiogram.contrib.middlewares.logging import LoggingMiddleware

from .acl import ACLMiddleware


def setup(dispatcher: Dispatcher):
    dispatcher.middleware.setup(LoggingMiddleware("bot"))
    dispatcher.middleware.setup(ACLMiddleware())
